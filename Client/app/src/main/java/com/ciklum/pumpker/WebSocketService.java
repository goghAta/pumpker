package com.ciklum.pumpker;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

public class WebSocketService extends Service {

    public static final String WS_WEBSOCKETHOST = "ws://pumpker1.herokuapp.com/socket";
    private WebSocketClient mWebSocketClient;
    private static final String TAG = WebSocketService.class.getSimpleName();

    public WebSocketService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate");
        connectWebSocket();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
        mWebSocketClient.close();
        mWebSocketClient = null;
    }

    private void connectWebSocket() {
        URI uri;
        try {
            uri = new URI(WS_WEBSOCKETHOST);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                Log.i(TAG, "Opened: " + serverHandshake.toString());
            }

            @Override
            public void onMessage(String message) {
                final String action = message.replaceAll("\"", "");
                final Intent intent = new Intent(getBaseContext(), FullScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setAction(action);
                getBaseContext().startActivity(intent);
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                Log.i(TAG, "Closed: " + s);
                connectWebSocket();
            }

            @Override
            public void onError(Exception e) {
                Log.i(TAG, "Error: " + e.getMessage());
            }
        };
        mWebSocketClient.connect();
    }
}
