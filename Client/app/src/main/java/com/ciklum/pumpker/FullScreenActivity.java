package com.ciklum.pumpker;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class FullScreenActivity extends AppCompatActivity {
    private static final String TAG = "Pumpker";

    private View mContentView;
    private final int contentViewDefaultColor = Color.BLACK;
    private MediaPlayer mMediaPlayer;
    private MediaRecorder mMediaRecorder;
    private final int mediaAnalyzeDelay = 10;
    private final int mediaAnalyzeMaxAmplitude = 5000;

    // http://www.freesfx.co.uk
    private final int[] soundResourceIds = {
            R.raw.darth_vader_breathing, // 0
            R.raw.evil_laughs,
            R.raw.scream_female,
            R.raw.scream_pain_male, // 30
            R.raw.chain_saw,
            R.raw.psycho_scream,
            R.raw.monster_roar,
            R.raw.crying_loud_male, // 11
            R.raw.child_scream, // 6
            R.raw.child_snorting_like_pig, // 7
            R.raw.comedy_kiss_001, // 8

            R.raw.laugh_hysterical_male, // 20
            R.raw.laugh_short_goofy_male, // 21

            R.raw.belching, // 1
            R.raw.hatoeeeee, // 12
            R.raw.human_flirty_wolf_whistle, // 13
            R.raw.human_mouth_fart_version_2, // 14
            R.raw.movie_line_godfather_mafia_boss_says_you_ve_come_to_the_right_guy_, // 28
            R.raw.male_says_wooh_reaction, // 25
            R.raw.male_sys_ooh_la_lah_reaction, // 26

            R.raw.scream_frustrated_male, // 29
            R.raw.scream_pain_male, // 30
            R.raw.snore_loud, // 31
            R.raw.snort_single_sniff, // 32

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_full_screen);

        mContentView = findViewById(R.id.fullscreen_content);
        setBackgroundColor(contentViewDefaultColor, 255);
        // Note that some of these constants are new as of API 16 (Jelly Bean)
        // and API 19 (KitKat). It is safe to use them, as they are inlined
        // at compile-time and do nothing on earlier devices.
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }


    }

    @Override
    protected void onNewIntent(Intent intent) {
        //am start -n com.ciklum.pumpker/.FullScreenActivity -a "soundId, soundVolume, color"
        if (!onNewCommand(intent.getAction())) {
            super.onNewIntent(intent);
        }
    }

    protected boolean onNewCommand(final String command) {
        boolean result = false;

        Log.i(TAG, "onNewCommand - '" + command + "'");

        final List<String> tokens = Arrays.asList(command.split("\\s*,\\s*"));
        if (tokens.size() == 3) {
            final String soundId = tokens.get(0);
            Log.i(TAG, "soundId - '" + soundId + "'");

            final String soundVolume = tokens.get(1);
            Log.i(TAG, "soundVolume - '" + soundVolume + "'");

            final String color = tokens.get(2);
            Log.i(TAG, "color - '" + color + "'");

            mMediaPlayer = MediaPlayer.create(this, soundResourceIds[Integer.parseInt(soundId)]);
            mMediaPlayer.start();

            mMediaRecorder = new MediaRecorder();
            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mMediaRecorder.setOutputFile("/dev/null");
            try {
                mMediaRecorder.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mMediaRecorder.start();

            mContentView.post(new Runnable() {
                @Override
                public void run() {
                    if (mMediaRecorder != null) {
                        final int amplitude = mMediaRecorder.getMaxAmplitude();
                        if (amplitude > 0) {
                            final int currentColor = ((ColorDrawable) mContentView.getBackground()).getColor();
                            final int newColorAlpha = Math.min(255, (int) (255 * (float) amplitude / mediaAnalyzeMaxAmplitude));
                            setBackgroundColor(currentColor, newColorAlpha);
                        }

                        mContentView.postDelayed(this, mediaAnalyzeDelay);
                    }
                }
            });

            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mMediaRecorder.stop();
                    mMediaRecorder.reset();
                    mMediaRecorder.release();
                    mMediaRecorder = null;

                    mMediaPlayer.stop();
                    mMediaPlayer.reset();
                    mMediaPlayer.release();
                    mMediaPlayer = null;

                    setBackgroundColor(contentViewDefaultColor, 255);
                }
            });

            final float parsedSoundVolume = Float.parseFloat(soundVolume);
            mMediaPlayer.setVolume(parsedSoundVolume, parsedSoundVolume);

            setBackgroundColor(Color.parseColor(color), 0);

            result = true;
        }

        return result;
    }

    protected void setBackgroundColor(final int color, final int alpha) {
        final int newColor = Color.argb(alpha, Color.red(color), Color.green(color), Color.blue(color));
        mContentView.setBackgroundColor(newColor);
    }
}
