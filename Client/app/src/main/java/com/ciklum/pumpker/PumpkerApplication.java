package com.ciklum.pumpker;

import android.app.Application;
import android.content.Intent;

/**
 * Created by Aleksei Zelikov on 10/30/15.
 */
public class PumpkerApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Intent intent = new Intent(this, WebSocketService.class);
        startService(intent);
    }
}
